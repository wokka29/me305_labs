'''!@file               Lab0x03_page.py
    @brief              Lab0x03 documentation page.
    @details            This page documents the Lab0x03.py file, which creates and controls motor objects through a motor driver object.

	@page page3			Lab 3 Deliverables
                        *Please see the files tab for file documentation
    @section sec_src3	Lab 3 source code
                        The source code files for Lab 3 can be found at https://bitbucket.org/wokka29/me305_labs/src/master/Lab%200x03/
    @section sec_intro3 Introduction
                        This program consists of seven files which work together to generate an encoder object from the encoder.py class file, record the time, position and speed data <br>
                        associated with that object iteratively in the taskEncoder.py file, and print the results to the console in the taskUser.py file. This last file, taskUser.py, <br>
                        also serves to handle user inputs which reset the encoder position, print the current position and speed of the encoder, and record position data for <br>
                        30 seconds or less. <br>
	@section sec_FSM3	Lab 3 Diagrams
    
    \htmlonly <style>div.image img[src="Lab0x03_taskSharing.jpg"]{width:893px;}</style> \endhtmlonly 
    @image html Lab0x03_taskSharing.jpg "Task Sharing Diagram for Lab0x03 - PMDC Motor Lab"
    
\htmlonly <style>div.image img[src="Lab3_taskUserFSM.jpg"]{width:1000px;}</style> \endhtmlonly 
@image html Lab3_taskUserFSM.jpg "Updated taskUser State Machine"

\htmlonly <style>div.image img[src="updated_taskEncoder_stateDiagram.jpeg"]{width:800px;}</style> \endhtmlonly 
@image html updated_taskEncoder_stateDiagram.jpeg "taskEncoder State Machine"

\htmlonly <style>div.image img[src="taskMotor.jpeg"]{width:800px;}</style> \endhtmlonly 
@image html taskMotor.jpeg "taskMotor State Machine"

@section sec_data Lab 3 Figures
    
\htmlonly <style>div.image img[src="Lab0x03_positionPlot.jpg"]{width:800px;}</style> \endhtmlonly 
@image html Lab0x03_positionPlot.jpg "Constant Velocity Motor Operation"

\htmlonly <style>div.image img[src="Lab0x03_velocityPlot.jpg"]{width:800px;}</style> \endhtmlonly 
@image html Lab0x03_velocityPlot.jpg "Encoder Data versus Theoretical Velocity"
    
@section sec_vid3	Demonstration of mulitple motor operation utilizing these drivers:
\htmlonly
<iframe title="vimeo-player" src="https://player.vimeo.com/video/680214656?h=d11ba8afb2" width="640" height="360" frameborder="0" allowfullscreen></iframe>
<p><a href="https://player.vimeo.com/video/680214656?h=d11ba8afb2">ME305 Motor Driver Lab</a> from <a href="https://player.vimeo.com/video/680214656?h=d11ba8afb2">Logan Williamson</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
\endhtmlonly
    
	
    @author              Logan Williamson
    @author              Brianna Roberts
    @date                February 21, 2022
'''
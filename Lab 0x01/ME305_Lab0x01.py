'''! @file ME305_Lab0x01.py
    @brief This code can change brightness of LED in apparent sequences upon button press
    @details Upon button press, LED shows different sequences: square, sine, and saw wave functions.
    Utilizes FSM state transition logic and integration. 
    @image html Lab0x01_FSM.jpg
    @author Logan Williamson
    @author Brianna Roberts
    @date 01/13/2022
    
'''
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 13 09:08:12 2022
'ME305_Lab0x01.py'
"""

import math
import time
import pyb
button_Pin = pyb.Pin (pyb.Pin.cpu.C13)
LED_Pin = pyb.Pin (pyb.Pin.cpu.A5)

def onButtonPressCallback(IRQ_src):
    global buttonPressed
    buttonPressed = True    #Button depressed flag variable
    
    '''! @brief Callback function connects button on pyboard to code
        @details When button is pressed on pyboard, 
                function is calledback and sets buttonpressed to boolean True 
    
        @param IRQ_src  Parameter that provides an interrupt request. This corresponds to the 
                        mechanical depression of the button
    '''

def SquareWave(t):
    '''! @brief This function calculates brightness at a given t value fluctuating in a square wave
        @details Function evaluates brightness percentage at given time for LED 
    
        @param t  Input value is time (t) from clock generated in main
        
        @return  Returns brightness level evaluated at time t for LED to appear fluctuating in a square wave
    '''
    
    trigger = t % 1000     # plugs in time value defined in state; modulus every 1000 ms start over trigger value
    if trigger <= 500:      # from 0 - 500 ms modulus would return 0-500
        brt = 100           # set LED brightness to 100%
    else:
        brt = 0             # set LED brightness to 0%
    return brt


def SineWave(t):
    
    '''! @brief Function for Sine Wave Generation
        @details Function evaluates brightness percentage at given time for LED 
    
        @param t  Input value is t from clock generated in main
        
        @return  Returns brightness level evaluated at time t for LED to appear fluctuating in a sine wave
    '''
   
    brt = 50*(math.sin(-2*3.1415*t/10000)+1) #sine function that returns brightness
    return brt

def SawWave(t):
    
    '''! @brief Function for Saw Tooth Wave Generation
        @details Function evaluates brightness percentage at given time for LED 
    
        @param t  Input value is t from clock generated in main
        
        @return  Returns brightness level evaluated at time t for LED to appear fluctuating in a sawtooth wave
    '''
    trigger = t % 1000  
    brt = trigger/10
    return brt
    
if __name__ == '__main__':
    print('Welcome! Please press the user Button B1 on your Nucleo Board to cycle through 3 different LED patterns!')
    ##  @brief  buttonPressed set to boolean values used to observe whether button on pyboard is pressed
    # @details buttonPressed is set to either true or false. 
    # Only set to true momentarily upon button press, once state is changed, 
    # button is immediately reset to False awaiting for next button press
    buttonPressed = False
    
    ##  @brief state used to transition between FSM state diagram, 4 states total
    # @details four states exist: 0, 1, 2, 3
    # states are transitioned upon button press.
    # state 0 is the "off" or initialization state. 
    # state 1 causes LED to transition to square wave function
    # state 2 causes LED to transition to sine wave function
    # state 3 causes LED to transition to saw wave function
    state = 0
    
    ## @brief ButtonInt connects button press with pyboard input
    # @details once button is pressed, initiates the onButtonPressCallback function
    # and turns on (sets True) the buttonPressed variable 
    

    ButtonInt = pyb.ExtInt(button_Pin, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressCallback)
    
    ## @brief tim2 creates variable for connecting timer from the pyboard 
    # @details used further in connecting to the LED pin
    tim2 = pyb.Timer(2, freq = 20000)
    
    ## @brief tim2 creates variable for connecting timer to the LED on the pyboard 
    # @details used to then change PWP on LED
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=LED_Pin)
    
    while True:
        try:
            if state == 0:
                # print('State Zero: off')
                
                if buttonPressed:
                    buttonPressed = False
                    #Assigns current count of time clock to be new startTime after state transition
                    ##  @brief startTime begins clock timer
                    #   @details startTime is reset at beginning of each state 
                    #   transition.
                    startTime = time.ticks_ms()     
                    state = 1
                    print('State 1: Square Wave')
                    
            elif state == 1:
                
                ## @brief t is the time variable used in each wave function
                #  @details 
                #
                t = time.ticks_diff(time.ticks_ms(),startTime) # Value that keeps counting continuously 
                                                                # time since we switched states

                ## @brief brt is the brightness calculated using the wave functions
                # @details brt (brightness) changes with each state transition 
                # it's a value between 0-100 given as a percentage. 
                # Used for LED to change the pulse width percent.
                brt = SquareWave(t)
                t2ch1.pulse_width_percent(brt) # return brightness from function
                
                if buttonPressed:
                    buttonPressed = False
                    #Assigns current count of time clock to be new startTime after state transition
                    startTime = time.ticks_ms()
                    state = 2
                    print('State 2: Sine Wave')
                
            elif state == 2:
                
                t = time.ticks_diff(time.ticks_ms(),startTime) # Value that keeps counting continuously 
                                                                # time since we switched states
                brt = SineWave(t)
                t2ch1.pulse_width_percent(brt) # return brightness from function
                
                if buttonPressed:
                    buttonPressed = False
                    #Assigns current count of time clock to be new startTime after state transition
                    startTime = time.ticks_ms()
                    state = 3
                    print('State 3: Sawtooth Wave')
                
            elif state == 3:
                
                t = time.ticks_diff(time.ticks_ms(),startTime)
                brt = SawWave(t)
                t2ch1.pulse_width_percent(brt)
                
                if buttonPressed:
                    buttonPressed = False
                    startTime = time.ticks_ms()
                    state = 1
                    print('State 1: Square Wave')
                    
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')

        
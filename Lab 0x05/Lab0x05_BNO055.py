"""!@file Lab0x05_DRV8847.py
    @brief A driver for controlling motors
    @details This file contains a class which can be called to generate a motor
    driver object. That object allows for control of any subordinate objects generated
    from a motor class via a single pulse-width modulation channel. This class also allows 
    the user to detect faults from, enable, or disable any subordinate motor objects.
    @author Logan Williamson
    @author Brianna Roberts
    @date 02/09/2022
"""

# from motor import Motor
from pyb import I2C, delay #, Pin, Timer, ExtInt,
from time import sleep_ms
import struct
import os

class BNO055:
    '''!@brief A motor driver class for the BNO055 from TI.
        @details Objects of this class can be used to configure the BNO055
        motor driver and to create one or more objects of the
        Motor class which can be used to perform motor
        control.
        Refer to the BNO055 datasheet here:
            https://cdn-learn.adafruit.com/assets/assets/000/036/832/original/BST_BNO055_DS000_14.pdf
            
        Device Calibration Information:
            https://learn.adafruit.com/adafruit-bno055-absolute-orientation-sensor/device-calibration
            
        bno055 Reference CODE:
            https://github.com/micropython-IMU/micropython-bno055
    '''
   
    
    def __init__ (self):
        '''!@brief Initializes and returns a BNO055 object.
            @details Upon calling the BNO055 class, the __init__ method will instantiate
            a motor driver object with the 
            timerPin, sleepPin, and faultPin pins passed in
            by the user.
        '''
        ##@brief MODE is the variable corresponding to the FUSION operating mode of the IMU
        #@details Passing in a value for MODE to the set_mode method enables the user to select 
        #one of the three available FUSION modes for the BNO055 Inertial Measurement Unit. 
        #MODE value 0 corresponds to configuration mode, which the BNO055 object is instantiated in.
        #MODE value 1 corresponds to IMU mode, 2 corresponds to NDOF mode, and 3 corresponds to M4G mode.
        self.bus_addr = 0x28
        self.cal_coeff_addr = 0x55
        self.vel_pos_data_addr = 0x14
        self.i2c = I2C(1,I2C.CONTROLLER)                 #Instantiates i2c bus object
        self.i2c.mem_write(0b0000,self.bus_addr,0x3D)    #Enables BNO055 in CONFIG mode
        self.MODE = 0
        delay(19)
        self.buf = bytearray(12)    # Preallocate position and velocity data byte array
        self.bufcal = bytearray(22) # Preallocate calibration coefficients byte array
        
    def set_mode(self,MODE):
        if MODE == 0:   #CONFIG mode
            self.MODE = 0
            self.i2c.mem_write(0b0000,self.bus_addr,0x3D)
            delay(19)
        elif MODE == 1:  #IMU mode
            self.MODE = 1
            self.i2c.mem_write(0b1000,self.bus_addr,0x3D)
            delay(7)
        elif MODE == 2: #NDOF mode
            self.MODE = 2
            self.i2c.mem_write(0b1100,self.bus_addr,0x3D)
            delay(7)
            
    def get_calStatus(self):
        
        self.cal_byte = self.i2c.mem_read(1,40,0x35)[0]
        
        # status for calibration
        self.mag_stat = (self.cal_byte & 0b00000011)
        self.acc_stat = (self.cal_byte & 0b00001100)>>2
        self.gyr_stat = (self.cal_byte & 0b00110000)>>4
        self.sys_stat = (self.cal_byte & 0b11000000)>>6
        
        return self.mag_stat, self.acc_stat, self.gyr_stat, self.sys_stat
    
    def get_calConstants(self): 
        self.cal_coeffs = self.i2c.mem_read(self.bufcal, self.bus_addr, self.cal_coeff_addr)
        return self.cal_coeffs
    
    def write_calConstants(self,calDATA):
        self.i2c.mem_write(calDATA,self.bus_addr,self.cal_coeff_addr)
        
    def update(self):
        # what is the adress for our peripherals
        self.i2c.mem_read(self.buf, self.bus_addr, self.vel_pos_data_addr)
        self.omega_y, self.omega_x, self.omega_z, self.theta_z, self.theta_x, self.theta_y = struct.unpack('<hhhhhh',self.buf)
        self.omega_y = -self.omega_y
        self.theta_x = -self.theta_x
        self.theta_z = -self.theta_z
        
    def get_data(self):
        return (self.theta_x,self.theta_y,self.theta_z,self.omega_x,self.omega_y,self.omega_z)

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.
    #bus_addr = 0x28                    # Physical bus address for i2c
    IMU = BNO055()                      # Instantiate BNO055 derived object, IMU, in CONFIG mode
    IMU.set_mode(2)                     # Set IMU to NDOF mode
    filename = "IMU_cal_coeffs.txt"     # Assign a filename for storing calibration coefficients
    
## Testing for calibration
    while True:
        IMU.update()
        #Create a .txt file containing the calibration coefficients if one doesn't already exist
        if filename in os.listdir():   # if file exists, read from it
            # open filename and 'r' read from it
            with open(filename, 'r') as f:
                # Read the first line of the file
                cal_data_string = f.readline()
                # Split the line into multiple strings and then convert each one to a float
            cal_coeffs = cal_data_string.split(',')
            for i in range(0, len(cal_coeffs)):
                cal_coeffs[i] = int(cal_coeffs[i],16)
                IMU.bufcal[i] = cal_coeffs[i]
            IMU.write_calConstants(IMU.bufcal)
            # IMU.get_calConstants()
            # print(IMU.cal_coeffs)

        else:
            ## These lines are for manual calibration if there is no .txt file present on the pybflash for bypassing this step.
            ## Currently commented out so we can just test writing to a .txt file, but will need to be reintroduced once we have 
            ## that writing to .txt file working so that we actually get the coefficients that we want.
            IMU.get_calStatus()
            
            sleep_ms(1000)
        # IMU in IMU mode
            if IMU.MODE == 1:
                print('acc_stat:',IMU.acc_stat,'gyr_stat:',IMU.gyr_stat)
                if IMU.acc_stat==3 and IMU.gyr_stat==3:
                    # run the get_calConstants() in IMU file
                    IMU.get_calConstants()
                    cal_coeffs = IMU.cal_coeffs
                    with open(filename, 'w') as f:
                        calString = f.write(','.join(hex(cal_coeff) for cal_coeff in cal_coeffs))
                        str_list = []
                    for cal_coeff in cal_coeffs:
                        str_list.append(hex(cal_coeff))
                        f.write(','.join(str_list))
        # IMU in NDOF mode
            elif IMU.MODE == 2:
                print('mag_stat:',IMU.mag_stat,'acc_stat:',IMU.acc_stat,'gyr_stat:',IMU.gyr_stat,'sys_stat:',IMU.sys_stat)
                if IMU.mag_stat==3 and IMU.acc_stat==3 and IMU.gyr_stat==3 and IMU.sys_stat==3:
                    IMU.get_calConstants()
                    cal_coeffs = IMU.cal_coeffs
                    print(f'Calibration complete. Calibration coefficients are: {cal_coeffs}')
                    str_list = []
                    for cal_coeff in cal_coeffs:
                        str_list.append(hex(cal_coeff))
                    with open(filename, 'w') as f:
                        f.write(','.join(str_list))
    
# ## Testing for positional and velocity output data
#     while True:
#         IMU.update()
#         IMU.get_data()
#         print('theta_x:',-IMU.theta_x/16,'theta_y:',IMU.theta_y/16,'theta_z:',-IMU.theta_z/16)
#         print('omega_x:',IMU.omega_x/16,'omega_y:',IMU.omega_y/16,'omega_z:',IMU.omega_z/16)
#         sleep_ms(1000)
    

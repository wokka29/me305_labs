"""!@file Lab0x05_closedLoop.py
    @brief Closed-loop driver module
    @details This file contains the class ClosedLoop, which implements closed-loop control 
    for a variety of systems and parameters within those systems. The ClosedLoop class is 
    to be used in conjunction with motor and encoder task files to produce the desired 
    closed-loop control behavior.
    @author: Logan Williamson
    @author: Brianna Roberts
    @date 02/22/2022
"""

class ClosedLoop:
    '''!@brief A closed-loop control driver
        @details Objects of this class can be used to take in input parameters representing 
        the measured and reference values and return the actuation signal after computing 
        the controller output.
    '''
    
    
#setpoint is Omega_ref
    def __init__(self,K_p,K_d,setpoint,posDATA,speedDATA):
        '''!@brief Initializes and returns a closed-loop controller object.
            @details Upon calling closedLoop.ClosedLoop, instantiates a closed-loop controller object.
            @param K_p The closed-loop controller gain
            @param setpoint The desired speed of the closed loop controller in [rad/s]
            @param speedDATA shares.Share variable representing the measured speed of an encoder object.
        '''
        
        self.theta_meas = posDATA
        # need to change later for ball balancing system
        self.theta_ref = 0
        self.Omega_meas = speedDATA
        # need to change later for ball balancing system
        self.Omega_ref = 0 
        
        self.K_p = K_p
        self.K_d = K_d
        self.L = 0
        #self.L = self.K_p*(self.theta_ref - self.theta_meas) + self.K_d*(self.Omega_ref - self.Omega_meas)
        
    def run(self, K_p, K_d, setpoint, posDATA, speedDATA):
        ##@brief Gain is the local interpretation of the K_p parameter within an object
        #@details Gain represents the closed-loop controller gain associated with a ClosedLoop object
        self.K_p = K_p
        self.K_d = K_d 
        
        self.theta_meas = posDATA
        ##@brief Omega_ref is the local interpretation of the setpoint parameter within an object
        #@details Omega_ref represents the closed-loop controller setpoint associated with a ClosedLoop object
        self.Omega_ref = setpoint
        ##@brief Omega_meas is the local interpretation of the speedDATA parameter within an object
        #@details Omega_meas represents the closed-loop controller speedDATA associated with a ClosedLoop object
        self.Omega_meas = speedDATA
        ##@brief L is the calculated duty cycle output of the controller.
        #@details L represents the duty cycle computed by the closed loop control algorithm to be passed into the 
        #taskMotor.py task for manipulation of the motor speed when the program is in closed-loop control mode.
        self.L = self.K_p*(self.theta_ref - self.theta_meas) + self.K_d*(self.Omega_ref - self.Omega_meas)
        
        return self.L
    
if __name__ == '__main__':
    controller = ClosedLoop(0.3,0,0)
"""!@file       Lab0x05_taskMotor.py
    @brief      Implementation of motor control as an FSM
    @details    This task instantiates and controls the behavior of a DRV8847 motor
                driver and its subordinate motor drivers. It does so by operating as a 
                finite state machine in coordiantion with the taskUser.py user interface.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       02/10/2022
"""
import Lab0x05_motor,micropython,Lab0x05_closedLoop
from pyb import Pin
from time import ticks_us,ticks_diff,ticks_add

##  @brief Creates object S0_INIT set to constant 0.
#   @details S0_INIT is used to initialize the FSM. Simply transitions from S0 
#   to S1.
S0_INIT = micropython.const(0)

##  @brief Creates object S1_RUN set to constant 1.
#   @details S1_INIT is the running state of the FSM. In this state, the user 
#   will be able to set the duty cycle for the motor objects instantiated by the 
#   DRV8847 class. The motors will run at the set duty cycle until a new duty 
#   cycle is selected, or a fault indication is detected.
S1_RUN = micropython.const(1)

##  @brief Creates object S3_CL set to constant 2
#   @details S2_CL is the closed loop control state for the motor(s). In this state, the
#   user will be unable to control the duty cycle (and associated speed) of the motor. 
#   Instead, this state will use user inputs for the closed-loop control gain value and 
#   a reference/setpoint value to indirectly control the motor speed.
S2_CL = micropython.const(2)

def taskMotorFcn(taskName,period,faultFlag,duty_y,duty_x,K_p,K_d,setpoint_x,setpoint_y,wFlag,posDATA,speedDATA):
    '''!@brief              A generator to implement the motor task as an FSM.
        @details            The task runs as a generator function and requires a 
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param faultFlag    Shared flag variable created so it can be passed to 
                            taskUser; this prompts the user to clear the fault before 
                            operation of the motors can be continued.
        @param duty1        Shared data for control of the duty cycle for the motor 1 object. This effectively
                            controls the speed of motor 1 because PMDC motors act as low pass filters.
        @param duty2        Shared data for control of the duty cycle for the motor 2 object. This effectively
                            controls the speed of motor 2 because PMDC motors act as low pass filters.
        @param K_p          Shared data for control of the duty cycle by means of closed-loop controller. This data is
                            the closed-loop gain factor.
        @param setpoint     Shared data for control of the duty cycle by means of closed-loop controller. This data
                            is used as the setpoint velocity desired by the user, which the closed loop controller uses to 
                            compute the difference between the setpoint and measured velocity of the motor.
        @param speedDATA    Shared data for control of the duty cycle by means of closed-loop controler. This data
                            is used as the measured velocity which the closed loop controller uses to compute
                            the difference between the setpoint and measured velocity of the motor.
    '''
    ## !motor_1 rotates in y+ direction when positive duty1 is entered
    motor_y = Lab0x05_motor.Motor(3, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    CL_ctr_y = Lab0x05_closedLoop.ClosedLoop(K_p.read(),K_d.read(),setpoint_y.read(),posDATA.read()[1],speedDATA.read()[1])
    
    ## !motor_2 rotates in x+ direction when positive duty2 is entered
    motor_x = Lab0x05_motor.Motor(3, Pin.cpu.B0, Pin.cpu.B1, 3, 4)
    CL_ctr_x = Lab0x05_closedLoop.ClosedLoop(K_p.read(),K_d.read(),setpoint_x.read(),posDATA.read()[0],speedDATA.read()[0])
    
    state = S0_INIT
    ##  @brief Creates object nextTime corresponding to the taskMotor next appointment.
    #   @details time clock value at which this task should run again. This is for the 
    #   purpose of task sharing between this task and taskUser, taskEncoder, etc.
    nextTime = ticks_add(ticks_us(),period)
    
    while True:
        currentTime = ticks_us()
        if ticks_diff(currentTime,nextTime) >= 0:
            nextTime += period
            
            if state == S0_INIT:
                    state = S1_RUN
      
            elif state ==  S1_RUN:
                    motor_y.set_duty(duty_y.read())
                    motor_x.set_duty(duty_x.read())
                    if wFlag.read() == True:
                        state = S2_CL
                    
            elif state == S2_CL:
                duty_x.write(CL_ctr_x.run(K_p.read(),K_d.read(),setpoint_x.read(),posDATA.read()[0],speedDATA.read()[0]))
                duty_y.write(CL_ctr_y.run(K_p.read(),K_d.read(),setpoint_y.read(),posDATA.read()[1],speedDATA.read()[1]))
                motor_y.set_duty(duty_y.read())
                motor_x.set_duty(duty_x.read())
                yield duty_y, duty_x
                if wFlag.read() == False:
                    duty_x.write(0)
                    duty_y.write(0)
                    yield duty_y,duty_x
                    state = S1_RUN
                    
            yield state
        else:
            yield None
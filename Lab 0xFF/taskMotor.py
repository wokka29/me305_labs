"""!@file       taskMotor.py
    @brief      Implementation of motor control as an FSM
    @details    This task instantiates and controls the behavior of two motor.Motor objects.
                It does so by operating as a finite state machine. This task file runs cooperatively
                with the taskUser.py user interface, taskPanel.py touchpanel task file, and taskIMU.py
                inertial measurement unit task file.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       03/17/2022
"""
import motor,micropython,closedLoop
from pyb import Pin
from time import ticks_us,ticks_diff,ticks_add

##  @brief      Creates object S0_INIT set to constant 0.
#   @details    S0_INIT is used to initialize the FSM. Simply transitions from S0 
#               to S1.
S0_INIT = micropython.const(0)

##  @brief      Creates object S1_RUN set to constant 1.
#   @details    S1_INIT is the running state of the FSM. In this state, the user 
#               will be able to set the duty cycle for the motor object(s). The motor(s) will 
#               run at the set duty cycle until a new duty cycle is selected.
S1_RUN = micropython.const(1)

##  @brief      Creates object S2_CL set to constant 2
#   @details    S2_CL is the closed loop control state for the motor(s). In this state, the
#               user will be unable to control the duty cycle (and associated speed) of the motor directly. 
#               Instead, this state will use closedLoop.ClosedLoop object(s) to control the motors.
S2_CL = micropython.const(2)

def taskMotorFcn(taskName,period,faultFlag,duty_y,duty_x,K_pi,K_di,K_po,K_do,setpoint_x,setpoint_y,wFlag,posDATA,speedDATA,DATA):
    '''!@brief              A generator to implement the motor task as an FSM.
        @details            The task runs as a generator function and requires a task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer number of microseconds.
        @param faultFlag    Flag variable shared with taskPanel. If the touchpanel.touchpanel object controlled by the 
                            taskPanel state machine does not detect contact, faultFlag is set True and the 
                            closedLoop.ClosedLoop objects controlling the motors will return the platform to the 
                            'home' (level) position.
        @param duty_y       Shared data for control of the duty cycle for the motor_y object. This effectively
                            controls the speed of motor_y because PMDC motors act as low pass filters.
        @param duty_x       Shared data for control of the duty cycle for the motor_x object. This effectively
                            controls the speed of motor_x because PMDC motors act as low pass filters.
        @param K_pi         Shared data used in a closedLoop.ClosedLoop controller object. This data is
                            the closed-loop proportional gain factor for the inner loop of a cascading closed-loop controller.
        @param K_di         Shared data used in a closedLoop.ClosedLoop controller object. This data is
                            the closed-loop derivative gain factor for the inner loop of a cascading closed-loop controller.
        @param K_po         Shared data used in a closedLoop.ClosedLoop controller object. This data is
                            the closed-loop proportional gain factor for the outer loop of a cascading closed-loop controller.
        @param K_do         Shared data used in a closedLoop.ClosedLoop controller object. This data is
                            the closed-loop derivative gain factor for the outer loop of a cascading closed-loop controller.
        @param setpoint_x   Shared data used in a closedLoop.ClosedLoop controller object. This data is used as the setpoint 
                            position desired by the user, which the closed loop controller uses to compute the difference 
                            between the setpoint and measured position readout from a touchpanel.touchpanel object.
        @param setpoint_y   Shared data used in a closedLoop.ClosedLoop controller object. This data is used as the setpoint 
                            position desired by the user, which the closed loop controller uses to compute the difference 
                            between the setpoint and measured position readout from a touchpanel.touchpanel object.
        @param wFlag        Shared flag variable with taskUser. Allows the user to toggle the operating state of taskMotor 
                            between manual to closed-loop operation.
        @param posDATA      Shared data used in a closedLoop.ClosedLoop controller object.  This data represents the angular 
                            position readout from a BNO055.BNO055 object in degrees.
        @param speedDATA    Shared data used in a closedLoop.ClosedLoop controller object. This data
                            is used as the measured angular velocity of a BNO055.BNO055 object which the closed loop 
                            controller uses to damp the motion generated by the controller.
        @param DATA         Shared data for control of the duty cycle by means of closed-loop controller. This data
                            contains the measured x- and y-position readouts from a touchpanel.touchpanel object.
    '''
    # For medium ball, use Kp_i = 8, Kd_i = 0.22, Kp_o = 0.1, Kd_o = 0.09
    # For large ball, use Kp_i = 12, Kd_i = 0.22, Kp_o = 0.1, Kd_o = 0.12
    
    ## !motor_1 rotates in y+ direction when positive duty1 is entered; therefore, positive duty1 yields positive x-motion on platform
    motor_x = motor.Motor(3, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    outer_ctr_x = closedLoop.ClosedLoop(0.1,0.09,setpoint_x.read(),DATA.read()[0],DATA.read()[2],0)
    inner_ctr_x = closedLoop.ClosedLoop(8,0.22,outer_ctr_x.L,posDATA.read()[1],speedDATA.read()[1],0)
    
    ## !motor_2 rotates in x+ direction when positive duty2 is entered; therefore, positive duty2 yields negative y-motion on platform
    motor_y = motor.Motor(3, Pin.cpu.B0, Pin.cpu.B1, 3, 4)
    outer_ctr_y = closedLoop.ClosedLoop(0.1,0.09,setpoint_y.read(),DATA.read()[1],DATA.read()[3],1.5)
    inner_ctr_y = closedLoop.ClosedLoop(8*1.15,0.22,outer_ctr_y.L,posDATA.read()[1],speedDATA.read()[1],2.5)
    
    state = S0_INIT
    ##  @brief Creates object nextTime corresponding to the taskMotor next appointment.
    #   @details time clock value at which this task should run again. This is for the 
    #   purpose of task sharing between this task and taskUser, taskEncoder, etc.
    nextTime = ticks_add(ticks_us(),period)
    
    while True:
        currentTime = ticks_us()
        if ticks_diff(currentTime,nextTime) >= 0:
            nextTime += period
            
            if state == S0_INIT:
                    state = S1_RUN
      
            elif state ==  S1_RUN:
                    motor_y.set_duty(duty_y.read())
                    motor_x.set_duty(duty_x.read())
                    if wFlag.read() == True:
                        state = S2_CL
                    
            elif state == S2_CL:
                if faultFlag.read() == False:
                    
                    #Run x-controller
                    outer_ctr_x.run(K_po.read(),K_do.read(),setpoint_x.read(),DATA.read()[0],DATA.read()[2],0)
                    if outer_ctr_x.L>15:
                        outer_ctr_x.L=15
                    elif outer_ctr_x.L<-15:
                        outer_ctr_x.L=-15
                    x_duty = inner_ctr_x.run(K_pi.read(),K_di.read(),outer_ctr_x.L,posDATA.read()[1],speedDATA.read()[1],0)
                    
                    #Run y-controller
                    outer_ctr_y.run(-K_po.read(),-K_do.read(),setpoint_y.read(),DATA.read()[1],DATA.read()[3],1.5)
                    if outer_ctr_y.L>15:
                        outer_ctr_y.L=15
                    elif outer_ctr_y.L<-15:
                        outer_ctr_y.L=-15
                    y_duty = inner_ctr_y.run(K_pi.read()*1.15,-K_di.read(),outer_ctr_y.L,posDATA.read()[0],speedDATA.read()[0],2.5)
                else:
                    #If faultFlag is true, no platform contact; set the desired angles to zero.
                    x_duty = inner_ctr_x.run(K_pi.read(),K_di.read(),0,posDATA.read()[1],speedDATA.read()[1],0)
                    y_duty = inner_ctr_y.run(K_pi.read(),K_di.read(),0,posDATA.read()[0],speedDATA.read()[0],2.5)
                if x_duty>40:
                    x_duty = 40
                elif x_duty<-40:
                    x_duty = -40
                else:
                    pass
                if y_duty>40:
                    y_duty = 40
                elif y_duty<-40:
                    y_duty = -40
                else:
                    pass
                motor_x.set_duty(x_duty)
                motor_y.set_duty(y_duty)
                
                # Store data in duty shares for printing/data processing
                duty_x.write(x_duty)
                duty_y.write(y_duty)
                
                yield duty_y, duty_x
                if wFlag.read() == False:
                    duty_x.write(0)
                    duty_y.write(0)
                    yield duty_y,duty_x
                    state = S1_RUN
                    
            yield state
        else:
            yield None
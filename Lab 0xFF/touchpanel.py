'''!@file       touchpanel.py
    @brief      A driver for reading from a four wire resistive touch panel
    @details    This file contains a class which can be called to instantiate touch panel objects.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       03/03/2022
'''
from pyb import ADC, Pin, Timer
import gc, array
class touchpanel:
    '''!@brief      A resistive touchpanel class.
        @details    Objects of this class can be used to iteratively read the x- and y-locations 
                    of applied pressure on a four wire resistive touchpanel. Additionally, detects whether 
                    or not there is no pressure input on the panel ('z-location')
    '''
    def __init__(self):
        '''!@brief      Initializes and returns an object associated with a resistive touch panel.
            @details    Upon initialization, sets all base objects to 
                        0 and or respective pins.The objects of this class 
                        are instantiated at 0 and then overwritten when the 
                        different methods are called. 
        '''
        
        # Measure x-position
        ##@brief    create object z_pos for the z position for touchpanel
        #@details   the z direction for the touchpanel is used to indicate if there
        #           is contact with the board. When z_pos = 0, contact with the board 
        #           is not registered. When z_pos = 1, contact with the board is 
        #           registered
        self.z_pos = 0
        
        ##@brief    create object x_pos for the x position for touchpanel
        #@details   the x object is initially set to 0 but then changed to the
        #           updated values of the registered x position when different
        #           methods are run. 
        self.x_pos = 0
        
        ##@brief    create object y_pos for the y position for touchpanel
        #@details   the y object is initially set to 0 but then changed to the
        #           updated values of the registered y position when different
        #           methods are run. 
        self.y_pos = 0
        
        ##@brief    creates a timer object tim using the Timer method 
        #@details   Timer object  PWM_tim passed in by user for the timer at a 
        #           preset frequency of 100_000
        self.timer = Timer(4,freq = 100_000)
        
        ##@brief    creates a pin object x_p set to 
        #@details   pin used to connect pin A7 configured for output with OUT_PP
        self.x_p = Pin(Pin.cpu.A7,Pin.OUT_PP)
        self.x_p.high()
        
        ##@brief    creates a pin object x_m set to 
        #@details   pin used to connect pin A1 configured for output with OUT_PP
        self.x_m = Pin(Pin.cpu.A1,Pin.OUT_PP)
        self.x_m.low()
        
        ##@brief    creates a pin object y_p set to 
        #@details   pin used to connect pin A6 configured for output with IN
        self.y_p = Pin(Pin.cpu.A6,Pin.IN)
        
        ##@brief    creates an ADC object y_m acting on a pin 
        #@details   pin used to connect pin A0 
        self.y_m = ADC(Pin(Pin.cpu.A0))
        self.y_m.read()
        
        ##@brief    creates an array object x_buf
        #@details   x_buf is an array of length 25 populated with 0's
        self.x_buf = array.array('h',25*[0])
        
        ##@brief    creates an array object y_buf
        #@details   y_buf is an array of length x_buf populated with 0's
        self.y_buf = array.array('h',self.x_buf)
        
        ##@brief    creates an array object z_buf
        #@details   z_buf is an array of length x_buf populated with 0's
        self.z_buf = array.array('h',self.x_buf)
        gc.collect()
        
    def pos_update(self):   
        '''!@brief      Updates positional data of touchpanel.
            @details    Upon calling the pos_update() method, scans x, y, 
                        and z positions by sets and resets the 
                        pins to OUT_PP and IN for measuring.
        '''

        # Measure x-position
        self.x_p = Pin(Pin.cpu.A7,Pin.OUT_PP)
        self.x_p.high()
        self.x_m = Pin(Pin.cpu.A1,Pin.OUT_PP)
        self.x_m.low()
        self.y_p = Pin(Pin.cpu.A6,Pin.IN)
        self.y_m = ADC(Pin(Pin.cpu.A0))
        # Read and filter x_pos
        self.y_m.read_timed(self.x_buf,self.timer)
        # self.x_pos = self.y_m.read()
        self.x_pos = sum(self.x_buf)/25
       
        # Check for any contact (z-position)
        self.x_p = Pin(Pin.cpu.A7,Pin.IN)
        #self.x_m = Pin(Pin.cpu.A1,Pin.OUT_PP)
        #self.x_m.low()9
        self.y_p = Pin(Pin.cpu.A6,Pin.OUT_PP)
        self.y_p.high()
        self.y_m = ADC(Pin(Pin.cpu.A0))
        self.y_m.read_timed(self.z_buf,self.timer)
        
        if sum(self.z_buf)/25 > 3995:
            self.z_pos = 0
            self.x_pos = 0
            self.y_pos = 0
            return self.z_pos
        elif sum(self.z_buf)/25 < 3995:
            self.z_pos = 1
        
        # Measure y-position
        #self.x_p = Pin(Pin.cpu.A7,Pin.IN)
        self.x_m = ADC(Pin(Pin.cpu.A1))
        #self.y_p = Pin(Pin.cpu.A6,Pin.OUT_PP)
        #self.y_p.high()
        self.y_m = Pin(Pin.cpu.A0,Pin.OUT_PP)
        self.y_m.low()
        # Read and filter y_pos
        self.x_m.read_timed(self.y_buf,self.timer)
        self.y_pos = sum(self.y_buf)/25
        
        ##@brief    creates a position object list of positions 
        #@details   position list in the order of x position, y position, and z position.
        self.position = (self.x_pos,self.y_pos,self.z_pos)
        return self.position
    
    # This code if for if we need to have separate x, y, z scan methods
    def scan_x(self):
        '''!@brief      Individually scans the x position 
            @details    Upon calling the scan_x() method, the x position can be 
                        individually scanned. This is already implemented in the 
                        pos_update method to run more efficiently, but for comprehension,
                        this method was also included. 
        '''
        xlist = 100*[0]
        self.x_p = Pin(Pin.cpu.A7,Pin.OUT_PP)
        self.x_p.high()
        self.x_m = Pin(Pin.cpu.A1,Pin.OUT_PP)
        self.x_m.low()
        self.y_m = ADC(Pin(Pin.cpu.A0))
        self.y_p = Pin(Pin.cpu.A6,Pin.IN)
    # filtering noise from position data
        for item in range(0,len(xlist)):
            xlist[item] = self.y_m.read()
        self.x_pos = sum(xlist)/len(xlist)
        return self.x_pos
            
    def scan_y(self):
        '''!@brief      Individually scans the y position 
            @details    Upon calling the scan_y() method, the y position can be 
                        individually scanned. This is already implemented in the 
                        pos_update method to run more efficiently, but for comprehension,
                        this method was also included. 
        '''
        ylist = 100*[0]
        self.y_p = Pin(Pin.cpu.A6,Pin.OUT_PP)
        self.y_p.high()
        self.y_m = Pin(Pin.cpu.A0,Pin.OUT_PP)
        self.y_m.low()
        self.x_p = Pin(Pin.cpu.A7,Pin.IN)
        self.x_m = ADC(Pin(Pin.cpu.A1))
    # filtering noise from position data
        for item in range(0,len(ylist)):
            ylist[item] = self.x_m.read()
        self.y_pos = sum(ylist)/len(ylist)
        return self.y_pos
    
    def scan_z(self):
        '''!@brief      Individually scans the z position 
            @details    Upon calling the scan_z() method, the z position can be 
                        individually scanned. This is already implemented in the 
                        pos_update method to run more efficiently, but for comprehension,
                        this method was also included. 
        '''
        self.x_p = Pin(Pin.cpu.A7,Pin.IN)
        self.x_m = Pin(Pin.cpu.A1,Pin.OUT_PP)
        self.x_m.low()
        self.y_p = Pin(Pin.cpu.A6,Pin.OUT_PP)
        self.y_p.high()
        self.y_m = ADC(Pin(Pin.cpu.A0))
        if self.y_m.read()<3995:
            self.z_pos = 1
            return self.z_pos
        elif self.y_m.read()>3995:
            self.z_pos = 0
            return self.z_pos

    
if __name__ == '__main__':
##Testing for pos_update() duration; must be under 1500[us]
    from time import ticks_us, ticks_diff
    panel = touchpanel()
    i = 0
    start_time = ticks_us()
    while True:
        time = ticks_us()
        if i < 99:
            panel.pos_update()
            i+=1
        elif i>=99:
            final_time = ticks_diff(time,start_time)
            print(final_time/100)
            break

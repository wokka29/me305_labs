'''!@file                mainpage.py
    @brief               Homepage for documentation repository and portfolio website.
    @details             This file serves as the seed file for Doxygen to generate a mainpage for my Bitbucket documentation repository.

    @mainpage
    @section sec_intro   Introduction
                           This documentation repository serves to organize and present my progress throughout the course of <br>
                           ME 305 - Intro to Mechatronics at California Polytechnic State University San Luis Obispo, CA. <br>
                           Documentation for each project completed in this course can be found under the 'Files' tab above. 
    @section sec_Projects Projects
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page1.html">Lab0x01 - Getting Started with Hardware</a>
                          \endhtmlonly
                          
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page2.html">Lab0x02 - Quadrature Encoders</a>
                          \endhtmlonly
                          
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page3.html">Lab0x03 - Permanent Magnet DC Motors</a>
                          \endhtmlonly
                          
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page6.html">Lab0x04 - Closed Loop Motor Control</a>
                          \endhtmlonly
                          
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page7.html">Lab0x05 - Inertial Measurement Units</a>
                          \endhtmlonly
                          
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page8.html">Term Project - Ball Balancing Platform</a>
                          \endhtmlonly
                          
    
    @section sec_Homework Background Calculations
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page4.html">Homework0x02 - Balancing Platform Analysis</a>
                          \endhtmlonly
                          
                          \htmlonly
                          <a href="https://wokka29.bitbucket.io/page5.html">Homework0x03 - Balancing Platform Simulation</a>
                          \endhtmlonly 
                          
    @section sec_mot     Motor Driver
                        A driver for creating and manipulating motor objects for control of PMDC motors.<br>
                        Please see motor.Motor for details.

    @section sec_enc     Encoder Driver
                        A driver that creates encoder object for reading position data from physical encoder.<br>
                        Please see encoder.Encoder for details.
                        
    @section sec_BNO     Inertial Measurement Unit Driver
                        A driver for reading data from the Adafruit BNO055 inertial measurement unit. This driver includes <br>
                        methods for calibrating and collecting data from a physical IMU. <br>
                        Please see BNO055.BNO055 for details.
                        
    @section sec_TP      Resistive Touchpanel Driver
                        A driver for reading data from a four-wire resistive touchpanel. This driver uses the STM32 Nucleo <br>
                        ADC to read the outputs from a resistive touchpanel and retrieve voltage data corresponding to locations<br>
                        on the phyiscal panel. <br>
                        Please see touchpanel.touchpanel for details.
                        
    @section sec_CL      Closed-Loop Controller Class
                        A driver for instantiating closed loop Proportional-Derivative controller objects.<br>
                        Please see closedLoop.ClosedLoop for details.                    

    @author              Logan Williamson

    @copyright           License Info

    @date                March 18, 2022
'''
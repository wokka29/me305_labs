'''!@file               HW0x02_page.py
    @brief              Homework 2 portfolio page.
    @details            This page contains the scanned images of our analysis for the ball balancing platform that we must write control code for as our ME 305 term project.

	@page page4			Ball Balancing Platform - Hand Calculations
    
    @section sec_intro3 Introduction
                        This analysis demonstrates the kinetics and kinematics required to model the motion associated with one degree of rotational freedom for a platform with a ball balanced on it. <br>
                        This analysis will be used in the ME 305 - Introduction to Mechatronics term project, which involves programming a platform with a pressure sensor on the surface to balance a ball. <br>
	@section sec_handCalcs	Analysis
    \htmlonly <style>div.image img[src="ME305_HW0X02_longform_jpg.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0X02_longform_jpg.jpg "Hand Calculations for the Kinetics and Kinematics of the Ball-Platform System"
    
	@author              Brianna Roberts
    @author              Logan Williamson
    @date                February 13, 2022
'''
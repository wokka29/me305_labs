'''!@file       Lab0x02_encoder.py
    @brief      A driver for reading from Quadrature Encoders
    @details    This file contains a class which can be called to generate a quadrature encoder object.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       02/12/2022
'''

import pyb
class Encoder:
    '''!@brief Interface with quadrature encoders
        @details When called, this class generates an encoder object and assigns the desired timer 
        and pins to it such that the position and change in position since the last update of the encoder position 
        can be determined and returned as outputs.
    '''
    
    def __init__(self,chA_pin,chB_pin,timNum):
        '''!@brief Constructs an encoder object.
            @details Upon calling the Encoder.py class, the __init__ function will run automatically. To run properly,
            this requires three inputs to the Encoder.py class call. There are: one input for each of the desired pins
            you wish to associate with the two encoder sensor channels. Additionally, the timer number to be used for 
            the encoder position count must be defined as the third input when calling this class. This function will 
            then instanciate an encoder object with the above parameters.
            @param chA_pin  creates empty object for an input of pin position. In this case we are using PB6 on the nucleo
            @param chB_pin  creates empty object for an input of pin position. In this case we are using PB7 on the nucleo
            @param timNum   creates an empty object for input timer. We will be using timer 4
        '''
        # These attributes are related to the hardware adresses being used
        
        ##  @brief tim creates new timer object for storing the encoder position.
        #   @details Runs timer 4 on the nucleo with a period of 2^16-1 and a prescaler of 0
        self.tim = pyb.Timer(timNum,period = 2**16-1, prescaler=0)
        
        ##  @brief creates channel 1 and channel 2 objects connected to the two pins input in the __init__ function
        #   @details ch1 and ch2 correspond to the inputs that will cause the encoder position to increment. This uses 
        #   ENC_AB configuration which puts the timer in encoder mode: The counter changes when CH1 or CH2 changes.
        #   The order in which they change determines whether the encoder position increments or decrements.
        self.ch1 = self.tim.channel(1,pyb.Timer.ENC_AB,pin=chA_pin)
        self.ch2 = self.tim.channel(2,pyb.Timer.ENC_AB,pin=chB_pin)
        
        # These attributes are related to positional data about the encoder
        ## @brief Sets base object of prevCount (previous position) to 0
        # @details This will be used in further calculations of delta, change in encoder position since the last update() call.
        self.prevCount = 0
        
        ## @brief position is the absolute position of the encoder.
        # @details position is initially set to zero and will be updated when update() runs
        # to reflect the absolute position of the encoder.
        self.position = 0
        
        print('New Encoder Object Created')
        
    def update(self):
        '''!@brief Updates encoder position and delta
            @details When called, this function will read and update the variable containing the encoder position, as
            well as the delta value of the encoder. This delta value will be analyzed for magnitude and direction to 
            determine whether an overflow or underflow in the encoder position count has occurred since the last update.
            In the event that the encoder position count has overflowed or underflowed, this update function will add or
            subtract the appropriate value from the encoder position count to reflect the true position of the encoder
            relative to its home position.
            @param self 
        '''
        #Evaluates the incremental position of the encoder when update is called
        
        self.count = self.tim.counter()   
        
        ## @brief delta is the difference between the current and previous positions
        # @details delta is used to test whether the counter has overflowed or not. It represents the change in count since the last time update() was called
        self.delta = self.count - self.prevCount
        
        if self.delta > 32768: # Max delta from (AR+1)/2 Overflow
            self.delta -= 65536
        elif self.delta < -32768: # Min Delta
            self.delta += 65536
        # Calculate position based on delta from the current call of self.update()    
        self.position += self.delta
        # Reset prevCount variable to the current count in preparation for the next time update is called
        self.prevCount = self.count
        
    def get_position(self):
        '''!@brief Returns encoder position
            @details If the user asks for the position, after position is calculated using update function, this function returns the calculated value
            @return The position of the encoder shaft
        '''
        return self.position
        
    def zero(self):
        '''!@brief Resets the encoder position to zero
            @details If user inputs the desire to reset the counter to zero, this function resets the encoder position to zero
        '''
        self.position = 0
        self.prevCount = self.tim.counter()
        
    def get_delta(self):
        '''!@brief Returns encoder delta
            @details if user asks for delta to be returned, this function will return delta value calculated from update funciton
            @return The change in position of the encoder shaft
            between the two most recent updates
        '''
        
        # do we need to correct the value if delta is too large?
        return self.delta
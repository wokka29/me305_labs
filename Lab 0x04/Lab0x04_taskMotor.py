"""!@file       taskMotor.py
    @brief      Implementation of motor control as an FSM
    @details    This task instantiates and controls the behavior of a DRV8847 motor
                driver and its subordinate motor drivers. It does so by operating as a 
                finite state machine in coordiantion with the taskUser.py user interface.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       02/10/2022
"""
import Lab0x04_DRV8847, Lab0x04_closedLoop, micropython
from pyb import Pin
from time import ticks_us,ticks_diff,ticks_add

##  @brief Creates object S0_INIT set to constant 0.
#   @details S0_INIT is used to initialize the FSM. Simply transitions from S0 
#   to S1.
S0_INIT = micropython.const(0)

##  @brief Creates object S1_RUN set to constant 1.
#   @details S1_INIT is the running state of the FSM. In this state, the user 
#   will be able to set the duty cycle for the motor objects instantiated by the 
#   DRV8847 class. The motors will run at the set duty cycle until a new duty 
#   cycle is selected, or a fault indication is detected.
S1_RUN = micropython.const(1)

##  @brief Creates object S2_FAULT set to constant 2.
#   @details S2_FAULT is the fault state for the motor(s). When the fault attribute
#   of the DRV8847 is True, this state is entered and the shared faultFlag variable 
#   is set to True. taskMotor will stay in this state until the user clears this fault 
#   i.e. resets the faultFlag to False.
S2_FAULT = micropython.const(2)

##  @brief Creates object S3_CL set to constant 3
#   @details S3_CL is the closed loop control state for the motor(s). In this state, the
#   user will be unable to control the duty cycle (and associated speed) of the motor. 
#   Instead, this state will use user inputs for the closed-loop control gain value and 
#   a reference/setpoint value to indirectly control the motor speed.
S3_CL = micropython.const(3)

def taskMotorFcn(taskName,period,faultFlag,duty1,duty2,K_p,setpoint,wFlag,speedDATA):
    '''!@brief              A generator to implement the motor task as an FSM.
        @details            The task runs as a generator function and requires a 
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param faultFlag    Shared flag variable created so it can be passed to 
                            taskUser; this prompts the user to clear the fault before 
                            operation of the motors can be continued.
        @param duty1        Shared data for control of the duty cycle for the motor 1 object. This effectively
                            controls the speed of motor 1 because PMDC motors act as low pass filters.
        @param duty2        Shared data for control of the duty cycle for the motor 2 object. This effectively
                            controls the speed of motor 2 because PMDC motors act as low pass filters.
        @param K_p          Shared data for control of the duty cycle by means of closed-loop controller. This data is
                            the closed-loop gain factor.
        @param setpoint     Shared data for control of the duty cycle by means of closed-loop controller. This data
                            is used as the setpoint velocity desired by the user, which the closed loop controller uses to 
                            compute the difference between the setpoint and measured velocity of the motor.
        @param speedDATA    Shared data for control of the duty cycle by means of closed-loop controler. This data
                            is used as the measured velocity which the closed loop controller uses to compute
                            the difference between the setpoint and measured velocity of the motor.
    '''
    motor_drv = Lab0x04_DRV8847.DRV8847(3, Pin.cpu.A15, Pin.cpu.B2)
    motor_1 = motor_drv.motor(3, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    motor_2 = motor_drv.motor(3, Pin.cpu.B0, Pin.cpu.B1, 3, 4)
    motor_drv.enable()
    
    CL_ctr = Lab0x04_closedLoop.ClosedLoop(K_p.read(),setpoint.read(),speedDATA.read())
    # CL_ctr = closedLoop.ClosedLoop(0,0,speedDATA.read())
    
    state = S0_INIT
    ##  @brief Creates object nextTime corresponding to the taskMotor next appointment.
    #   @details time clock value at which this task should run again. This is for the 
    #   purpose of task sharing between this task and taskUser, taskEncoder, etc.
    nextTime = ticks_add(ticks_us(),period)
    
    while True:
        currentTime = ticks_us()
        if ticks_diff(currentTime,nextTime) >= 0:
            nextTime += period
            
            if state == S0_INIT:
                    state = S1_RUN
      
            elif state ==  S1_RUN:
                # why isn't there a .write here?
                    motor_1.set_duty(duty1.read())
                    motor_2.set_duty(duty2.read())
                    if wFlag.read() == True:
                        state = S3_CL
                    if motor_drv.faultStatus == True:
                        faultFlag.write(True)
                        state = S2_FAULT
                
            elif state == S2_FAULT:
                if faultFlag.read() == False:
                    motor_drv.faultStatus = False
                    motor_drv.enable()
                    state = S1_RUN
                    
            elif state == S3_CL:
                CL_ctr.run(K_p.read(), setpoint.read(),speedDATA.read())
                motor_1.set_duty(CL_ctr.L)
                duty1.write(CL_ctr.L) 
                yield duty1
                
                # print(CL_ctr.L)
                if wFlag.read() == False:
                    state = S1_RUN
                if motor_drv.faultStatus == True:
                    faultFlag.write(True)
                    state = S2_FAULT
            yield state
        else:
            yield None
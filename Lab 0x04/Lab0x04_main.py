"""!@file Lab0x04_main.py
    @brief Main script of ME305 Lab0x04. 
    @details This file instantiates the shared flag variables, shared data variables, 
    and controls the task sharing between taskEncoder.py and taskUser.py.
    @author: Logan Williamson
    @author: Brianna Roberts
    @date 01/27/2022
"""

import shares, Lab0x04_taskUser, Lab0x04_taskEncoder, Lab0x04_taskMotor

# Flags:
    
##  @brief Create object zFlag shared between all files.
#   @details Used as a flag between files to communicate when z or Z is pressed. 
#   also used in order to communicate between tasks.
zFlag = shares.Share(False)

##  @brief Create object zFlag shared between all files.
#   @details Used as a flag between files to communicate when z or Z is pressed. 
#   also used in order to communicate between tasks.
faultFlag = shares.Share(False)

##  @brief Create object wFlag shared between all files.
#   @details Used as a flag between files to communicate when the program is in closed- 
#   or open-loop control mode. When wFlag is False, the motor speed is being set by 
#   user input (open-loop control). When wFlag is True, the motor speed is set by gain 
#   and setpoint values specified by the user.
wFlag = shares.Share(False)



''' Shared Variables from ENCODER '''

##  @brief Create an object timDATA shared between all files
#   @details This is the value of time used in taskEncoder to keep track of
#   time. Set to timDATA.write(ticks_ms()-init_time)
timDATA = shares.Share(0)

##  @brief Create an object posDATA shared between all files
#   @details This is the value of position that is written in the taskEncoder 
#   and printed in the taskUser when prompted
posDATA = shares.Share(0) 

##  @brief Create an object delDATA shared between all files
#   @details This is the value of delta that is written in the taskEncoder 
#   and printed in the taskUser when prompted
delDATA = shares.Share(0)

##  @brief Create an object DATA shared between all files.
#   @details This is a tuple that records the printing data. Used when 
#   user prompts to ask for 30 second data collection. This is shared with taskUser
#   and formatted to print in an array. 
DATA = shares.Share(0)

##  @brief Create an object soeedDATA shared between all files.
#   @details This is a tuple that records the data and the position. used when 
#   user prompts to ask for 30 second data collection. This is shared with taskUser
#   and formatted to print in an array. 
speedDATA = shares.Share(0)

''' Shared Variables from MOTOR '''
##  @brief Create an object duty1 shared between all files.
#   @details This is a value that will be passed into the motor.Motor objects 
#   for the purpose of setting the duty cycle (and by extension, the speed) of 
#   the
duty1 = shares.Share(0)

##  @brief Create an object DATA shared between all files.
#   @details This is a tuple that records the data and the position. used when 
#   user prompts to ask for 30 second data collection. This is shared with taskUser
#   and formatted to print in an array. 
duty2 = shares.Share(0)

''' Shared Variables from closedLoop '''
##  @brief Create a shared object K_p corresponding to closed-loop controller gain.
#   @details This is a shares.Share object that enables the user to input a closed-loop 
#   controller gain.
K_p = shares.Share(0)

##  @brief Create a shared object setpoint corresponding to the desired motor speed.
#   @details This is a shares.Share object that sets the desired motor speed in rad/s. 
#   The values are limited to -175 to 175 rad/s; values outside this range will be 
#   'rounded' to the nearest limit on this range upon input.
setpoint = shares.Share(0)

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    taskList = [Lab0x04_taskUser.taskUserFcn ('Task User', 10_000, zFlag, timDATA, posDATA, delDATA, DATA, speedDATA, faultFlag, duty1, duty2, K_p, setpoint, wFlag),
                    Lab0x04_taskEncoder.taskEncoderFcn ('Task Encoder', 10_000, zFlag, timDATA, posDATA, delDATA, DATA, speedDATA),
                    Lab0x04_taskMotor.taskMotorFcn ('Task Motor', 10_000, faultFlag, duty1, duty2, K_p, setpoint, wFlag, speedDATA)]
    
        
    while True: 
        try:
            for task in taskList:
                next(task)
                
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
        
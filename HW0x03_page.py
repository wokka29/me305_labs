'''!@file               HW0x03_page.py
    @brief              Homework 3 portfolio page.
    @details            This page contains the scanned images of our MATLAB script for simulation of the ball balancing platform used in the ME 305 term project.

	@page page5			Ball Balancing Platform - Simulation
    
    @section sec_intro3 Introduction
                        This MATLAB simulation demonstrates the system response associated with one degree of rotational freedom for a platform with a ball balanced on it. <br>
                        This analysis will be used in the ME 305 - Introduction to Mechatronics term project, which involves programming a platform with a pressure sensor on the surface to balance a ball. <br>
	@section sec_Sim	MATLAB Simulation Main Script
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0001.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0001.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0002.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0002.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0003.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0003.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0004.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0004.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0005.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0005.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0006.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0006.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0007.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0007.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0008.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0008.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0009.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0009.jpg
    \htmlonly <style>div.image img[src="ME305_HW0x03_page-0010.jpg"]{width:1000px;}</style> \endhtmlonly 
    @image html ME305_HW0x03_page-0010.jpg
    
    @author              Logan Williamson
	@author              Brianna Roberts
  
    @date                February 21, 2022
'''
'''!@file               Lab0x04_page.py
    @brief              Lab0x04 documentation page.
    @details            This page documents the Lab0x04.py file, which creates and controls motor objects through a motor driver object.

	@page page6			Lab 4 Deliverables
                        *Please see the files tab for file documentation
    @section sec_src6	Lab 4 source code
                        The source code files for Lab 4 can be found at https://bitbucket.org/wokka29/me305_labs/src/master/Lab%200x04/
    @section sec_intro6 Introduction
    This program consists of nine files which together generate and present a user interface for control of and data collection from two small permanent magnet DC motors connected <br>
    to an STM32 Nucleo development board. Data collection in this program is performed using an encoder object from the encoder.py class file, which records position and speed data <br>
    associated with the encoder iteratively in the taskEncoder.py file, and prints the results to the console via shares.Share variables passed to the taskUser.py file. Additionally, <br>
    this program generates two motor.Motor objects from the motor.py class file, which are used to control the operation of two physical motors connected to the STM32 Nucleo board used for <br>
    this project. Higher level control of these motor objects is performed by a motor driver object instantiated from the DRV8847.py class which can be used to enable and disable <br>
    operation of the subservient motor objects in this class heirarchy. The DRV8847.DRV8847 and motor.Motor objects are instantiated and managed by the taskMotor.py task file. <br>
    Finally, this program instantiates a closed-loop controller object from the closedLoop.py class within the taskMotor task as well. This object serves as a closed-loop controller <br>
    which allows the user to set controller parameters for managing the motor operation rather than directly controlling the motor duty cycle with user input (i.e. open-loop control).<br>
    Management of multitasking between the taskUser.py, taskEncoder.py, and taskMotor.py task files is handled within the main.py file.<br>

	@section sec_FSM6	Lab 4 Diagrams

    @image html Lab0x04_taskSharing.jpg "Lab0x04 updated Task Sharing Diagram"

    @image html Lab0x04_taskUser_FSM.jpeg "Lab0x04 updated taskUser.py Finite State Machine"

    @image html Lab0x04_taskMotor_FSM.jpeg "Lab0x04 updated taskMotor.py Finite State Machine"

    @image html Lab0x04_taskEncoder_FSM.jpeg "Lab0x04 taskEncoder.py Finite State Machine"

    @image html Lab0x04_blockDiagram.jpg "Lab0x04 Closed-Loop Motor Controller Block Diagram"

    @section sec_data6  Lab 4 Figures

    @image html Lab0x04_StepPlot_1.png "Step Response Plot 1: Maximum Reference Velocity, Small Gain"

    @image html Lab0x04_StepPlot_2.png "Step Response Plot 2: Maximum Reference Velocity, Medium Gain"

    @image html Lab0x04_StepPlot_3.png "Step Response Plot 3: Maximum Reference Velocity, Large Gain"

    @image html Lab0x04_StepPlot_4.png "Step Response Plot 4: Medium Reference Velocity, Medium Gain"

    @image html Lab0x04_StepPlot_5.png "Step Response Plot 5: Maximum Reference Velocity, Medium Gain"

    @section sec_vid6	Demonstration of Step Response Using Closed-Loop Control:
    \htmlonly
    <iframe src="https://player.vimeo.com/video/682359254?h=80685dd887" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/682359254">ME305_Lab0x04</a> from <a href="https://vimeo.com/user164261385">Brianna Roberts</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
    \endhtmlonly
	
    @author              Logan Williamson
    @author              Brianna Roberts
    @date                February 21, 2022
'''
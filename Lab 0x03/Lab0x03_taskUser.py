'''!@file       Lab0x03_taskUser.py
    @brief      Implimentation of User Interface task as an FSM
    @details    The task uses the USB VCP (Virtual COM Port) to take character input 
                from the user working at a serial terminal such as PuTTY. It uses 
                this input with Finite State Machine logic to determine which functionality
                should be executed based on that user input. This file works cooperatively
                with taskEncoder.py to allow both files' functionality to be executed quasi-
                simultaneously.
                
    @author     Brianna Roberts
    @author     Logan Williamson
    @date       01/26/2022
'''
import micropython, array, gc
from time import ticks_us, ticks_add, ticks_diff
from pyb  import USB_VCP

##  @brief Creates object S0_INIT connected to constant 0
#   @details S0_INIT is used as an initialization state
#   it prints the help interface block and sends FSM to state 1 (S1_Wait)
S0_INIT = micropython.const(0)

##  @brief Creates object S1_Wait connected to constant 1
#   @details S1_Wait is used as an wait state. Used to decode what the user is 
#   inputting. sends user to respective states given specific keyboard inputs
S1_Wait = micropython.const(1)

##  @brief Creates object S2_zZeroEnc1 connected to constant 2
#   @details S2_zZeroEnc1 is used as a zeroing state. If user inputs z or Z,
#   they are sent to this state which zero's the encoder from the taskEncoder
#   uses zFlag to communicate between all files
S2_zZeroEnc1 = micropython.const(2)

##  @brief Creates object S3_pPrintPos connected to constant 3
#   @details S3_pPrintPos is used as a printing state. If user inputs p or P,
#   they are sent to this state which prints ONE value of the current position 
#   from the encoder. uses POS to connect between files
S3_pPrintPos = micropython.const(3)

##  @brief Creates object S4_dDelta connected to constant 4
#   @details S4_dDelta is used as a delta state. If user inputs d or D,
#   they are sent to this state which prints ONE value of the current delta value
#   from the encoder. uses DEL to connect between files
S4_dDelta = micropython.const(4)

##  @brief Creates object S5_vVelocity connected to constant 5
#   @details S5_vVelocity is used as a velocity state. If user inputs v or V,
#   they are sent to this state which prints ONE value of the current velocity value
#   from the encoder. uses speedDATA to connect between files
S5_vVelocity = micropython.const(5)

##  @brief Creates object S6_mDutyCtrl connected to constant 6
#   @details S6_mDutyCtrl is used to set the duty cycle of motor 1. 
#   If user inputs m, they are sent to this state which prompts the user to 
#   input their desired duty cycle. Uses variable duty1 to connect between files
S6_mDutyCtrl = micropython.const(6)

##  @brief Creates object S8_clrFault connected to constant 8
#   @details S8_clrFault is used to check if there is a fault detected by the
#   driver. Uses flag variable with boolean logic. flag used is faultFlag
S7_clrFault = micropython.const(7)

##  @brief Creates object S8_gCollectThirtySec connected to constant 9
#   @details S8_gCollectThirtySec is used to collect data from encoder
#   continuously for 30 seconds. The values are then printed out all together
#   in an array. If the user decides to end data collection early, they may
#   do this by entering s or S on the keyboard.
S8_gCollectThirtySec = micropython.const(8)

##  @brief Creates object S9_tTestInt connected to constant 9
#   @details S9_tTestInt is used to repeatedly prompt the user for duty cycles 
#   for motor 1.  Determines average velocity for each prompted duty cycle. 
#   these values (duty cycle, average velocity) are then printed out in array
#   once user is done testing, in which they type s or S on the keyboard.
S9_tTestInt = micropython.const(9)

##  @brief Creates object S10_PrintData connected to constant 10
#   @details S10_PrintData is used to print the compiled data that has been 
#   collected from both Encoder data or Testing data. 
S10_PrintData = micropython.const(10)

##  @brief Creates object S11_testingDutySet connected to constant 11
#   @details S11_testingDutySet is used to set the duty cycle of motor 1 while 
#   in the testing interface. Upon 't' press, the program is sent to this state which 
#   prompts the user to input their desired duty cycle. Uses variable duty1 to pass 
#   the desired duty cycle to taskMotor.py
S11_testingDutySet = micropython.const(11)

##  @brief Creates object S12_testingDataCrunch connected to constant 12
#   @details S12_testingDataCrunch is the state used to collect a sample of velocity
#   data points and use them to compute the average velocity of that set. This duty cycle
#   is then written to posArray and the averaged velocity value is written to velArray 
#   before being used to print a dataset representing speed as a function of duty cycle.
S12_testingDataCrunch = micropython.const(12)

def taskUserFcn(taskName, period, zFlag, timDATA, posDATA, delDATA, DATA, speedDATA, faultFlag, duty1, duty2):
    '''!@brief              A generator to implement the UI task as an FSM.
        @details            The task runs as a generator function and requires a
                            task name and interval to be specified.
        @param taskName     The name of the task as a string.
        @param period       The task interval or period specified as an integer
                            number of microseconds.
        @param zFlag        Shared flag variable which causes FSM to enter zeroing state.
        @param timDATA      Shared data for the time during a data recording session. This is not currently utilized by 
                            taskUser.py (instead, it is immediately fed into DATA); however, this is shared to taskUser
                            for debugging purposes.
        @param posDATA      Shared data for printing the position of the encoder upon 'p' or 'P' press.
        @param delDATA      Shared data for printing the delta value of the encoder upon 'd' or 'D' press.
        @param DATA         Shared data for data recording upon 'g' or 'G' press.     
        @param speedDATA    Shared data for printing the velocity of the encoder upon 't' or 'T' press.
        @param faultFlag    Shared flag variable for indicating when a fault condition has occurred.
        @param duty1        Shared data for controlling the duty cycle of the motor_1 object, effectively controlling its speed.
        @param duty2        Shared data for controlling the duty cycle of the motor_2 object, effectively controlling its speed.
    '''
    
    ##  @brief S1_Wait is the holding state that keeps the program open and checking
    #   for user input.
    #   @details This object represents the first state in our Finite State Machine.
    #   In this state, the taskUser checks the serial port object for any waiting
    #   user input characters. This state contains the logic which can transition the
    #   FSM to other states if a valid user input character is entered.
    state = S0_INIT
    
    ##  @brief start_time which starts the clock.
    #   @details A timestamp, in microseconds, indicating the current time for 
    #   the run.
    start_time = ticks_us()
    
    ##  @brief next_time sets an appointment time for the next run.
    #   @details A timestamp, in microseconds, indicating when the next iteration of the
    #   generator must run.
    next_time = ticks_add(start_time, period)
    
    ##  @brief serport is a (virtual) serial port object.
    #   @details A (virtual) serial port object used for reading character inputs
    #   during the cooperative operation of our taskUser state machine with its taskEncoder
    #   counterpart.
    serport = USB_VCP()
    
    ##  @brief numItems tracks the number of data points collected in State 5.
    #   @details This object represents the number of data points that have been passed
    #   to taskUser from taskEncoder while taskUser is in the data collection state,
    #   S5_gCollectThirtySec. This object is incremented as each data point is passed in
    #   until it is equal to the desired number of data points defined by totItems.
    numItems = 0
    
    ##  @brief totItems determines how many data points to gather during collection.
    #   @details This object represents the maximum value that numItems can increment
    #   to as data is being recorded in State 5, S5_gCollectThirtySec. This corresponds
    #   to the hundredths of a second; in other words, totItems = 3001 will dictate a 
    #   data collection period of 30 seconds, with one data point every 0.01 seconds.
    totItems = 3001
    
    ##  @brief numPrint tracks the number of data points printed in State 6.
    #   @details This object represents the number of data points that have been
    #   printed while in State 6, S6_PrintData. Once this value reaches the value
    #   of numItems, all the data collected has been printed and the FSM transitions
    #   back to State 1, S1_Wait. This condition also resets numItems and clears the
    #   timeArray and posArray in preparation for the next instance of data collection.
    numPrint = 0
    
# State 9 testing interface indexing control values
    ##  @brief testItems determines how many data points to gather and average during testing.
    #   @details This object represents the maximum value that numItems can increment
    #   to as data is being recorded in State 9, S9_tTestInt. This corresponds
    #   to the hundredths of a second; in other words, testItems = 50 will dictate a 
    #   data collection period of 0.2 seconds, with one data point every 0.01 seconds.
    testItems = 100
    
    ##  @brief numItemsTestPrint tracks the number of data points collected in State 9.
    #   @details This object represents the number of data points that have been passed
    #   to taskUser from taskEncoder while taskUser is in the testing state,
    #   S9_tTestInt. This object is incremented as each duty cycle and average velocity
    #   data point is passed in until the user exits the testing interface. At this point,
    #   numItemsTest is reset to zero in preparation for future testing sessions.
    numPrintTest = 0
    
# Data storage arrays for testing and printing states
    ##  @brief posArray is the array in which collected encoder position data is stored.
    #   @details This object is an array which will store the encoder position 
    #   data generated during data collection in State 5, S5_gCollectThirtySec.
    #   This data is indexed from the shared parameter, DATA.
    posArray = array.array('f',totItems*[0])
    gc.collect()
    ##  @brief timeArray is the array in which collected time data is stored.
    #   @details This object is an array which will store the time data generated 
    #   during data collection in State 5, S5_gCollectThirtySec. This data is indexed
    #   from the shared parameter, DATA.
    timeArray = array.array('f',posArray)
    gc.collect()
    
    ##  @brief velArray is the array in which collected motor velocity data is stored.
    #   @details This object is an array which will store the velocity data generated during
    #   data collection in State 5, S5_gCollectThirtySec. This data is indexed from the shared
    #   parameter, DATA.
    velArray = array.array('f',posArray)
    gc.collect()

    ##  @brief creating mFlag variable used to simplify state diagram.
    #   @details mFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #   cycle. When this flag is raised, it will change duty cycle of motor 1.
    mFlag = False
    
    ##  @brief creating mFlag variable used to simplify state diagram.
    #   @details mFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S6_mDutyCtrl. It is used to set the correct duty 
    #   cycle. When this flag is raised, it will change duty cycle of motor 1.
    MFlag = False
    
    ##  @brief creating encDataFlag variable used to simplify state diagram.
    #   @details encDataFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S10_PrintData.  This flag causes print state to print 
    #   encoder data from S8_gCollectThirtySec after user manually quits using
    #   s or S or after 30 seconds of data collection is up. 
    encDataFlag = False
    
    ##  @brief Create testDataFlag variable used to simplify state diagram.
    #   @details testDataFlag is NOT a shared variable. It is used to communicate 
    #   between S1_Wait and  S10_PrintData.  This flag causes print state to print 
    #   Duty data from testDataFlag after user manually quits using
    #   s or S or after 30 seconds of data collection is up. 
    testDataFlag = False
    
    ##  @brief Create a variable that indicates whether or not the user has pressed enter
    #   @details The enter variable is used to indicate to the taskUser script whether or not
    #   the user has pressed enter to set the duty cycle for motor_1 or motor_2. This controls
    #   whether the program is waiting for additional duty cycle characters or is complete and
    #   should send the entered duty cycle to the motor driver.
    enter = False
    
    ##  @brief Local variable that blocks multiple prints of fault detection message
    #   @details faultPrintBlocker is a logical value that will block the fault detection
    #   message from printing numerous times in the user interface. When a fault is triggered,
    #   the message will print once and faultPrintBlocker will be set to True until the fault is
    #   cleared.
    faultPrintBlocker = False
    
    # The finite state machine must run indefinitely.
    while True:
        ##  @brief current_time tracks the time in microseconds.
        #   @details This object tracks the elapsed time since taskUser was first called
        #   in order to manage task sharing between taskUser and taskEncoder. It is used
        #   in conjunction with the designated period parameter to set 'appointments' for 
        #   the next run of taskUser and prevent it from dominating task sharing.
        current_time = ticks_us()
        
        # FSM only needs to run if interval has elapsed. must use ticks_diff() b/c of overflow
        if ticks_diff(current_time,next_time) >= 0:
        
            # reset next_time to appointment for next iteration; need ticks_add for overflow 
            next_time = ticks_add(next_time, period)
            
            #state 0 code
            if state == S0_INIT:
                # On the entry to state 0 we can print a message for the user describing the
                # UI.
                print("+-----------------------------------------------------------------+")
                print('| Command  | Description                                          |')
                print("+-----------------------------------------------------------------+")
                print('|  z or Z  |  Zero the position of encoder 1                      |')
                print('|  p or P  |  Print the position of encoder 1                     |')
                print('|  d or D  |  Print the delta (speed) of encoder 1                |')
                print('|  v or V  |  Print out velocity of encoder 1                     |')
                print('|    m     |  Enter specified duty cycle for motor 1              |')
                print('|    M     |  Enter specified duty cycle for motor 2              |')
                print('|  c or C  |  Clear a fault condition triggered by the DRV 8847   |')
                print('|  g or G  |  Collect data from encoder 1 for 30 seconds          |')
                print('|  t or T  |  Start Testing Interface                             |')
                print('|  s or S  |  End data collection prematurely                     |')
                print('|  h or H  |  Print this help message                             |')
                print('|  Ctrl-C  |  Terminate Program                                   |')
                print("+-----------------------------------------------------------------+")
                
                state = S1_Wait
                
# State 1 code - Base state for awaiting user input
            elif state == S1_Wait: 
                testDataFlag = False
                if faultFlag.read()==True:
                    if faultPrintBlocker == False:
                        print('Fault Detected! Press (c) to clear')
                        faultPrintBlocker = True
                    
                # If a character is waiting, read it
                if serport.any():
                    ##  @brief charIn is the variable where input characters are stored after reading
                    #   and decoding them.
                    #   @details Reads one character and decodes it into a string. This
                    #   decoding converts the bytes object to a standard Python string object.
                    charIn = serport.read(1).decode()
                    
                    if charIn in {'z', 'Z'}:
                        print("Zeroing the position")
                        zFlag.write(True)
                        state = S2_zZeroEnc1 # transition to state 2
                        
                    elif charIn in {'p', 'P'}:
                        print('Recieving Position Value')
                        state = S3_pPrintPos # transition to state 3
                        
                    elif charIn in {'d', 'D'}:
                        print('Recieving Delta Value')
                        state = S4_dDelta # transition to state 4
                        
                    elif charIn in {'v', 'V'}:
                        print('Recieving Velocity Value')
                        state = S5_vVelocity # transition to state 4
                    
                    elif charIn == 'm':
                        print("Please Enter a Duty Cycle between -100 and 100: ")
                        mFlag = True
                        state = S6_mDutyCtrl
                        
                    elif charIn == 'M':
                        print("Please enter a duty cycle between -100 and 100:")
                        MFlag = True
                        state = S6_mDutyCtrl
                        
                    elif charIn in {'c', 'C'}:
                        faultPrintBlocker = False
                        state = S7_clrFault
                        
                    elif charIn in {'g', 'G'}:
                        print('Collecting data for 30 seconds')
                        encDataFlag = True
                        state = S8_gCollectThirtySec 
                        
                    elif charIn in {'t', 'T'}:
                        print('Entering Motor Testing Mode')
                        testDataFlag = True
                        state = S9_tTestInt
                        
                    elif charIn in {'h', 'H'}: 
                        print("+-----------------------------------------------------------------+")
                        print('| Command  | Description                                          |')
                        print("+-----------------------------------------------------------------+")
                        print('|  z or Z  |  Zero the position of encoder 1                      |')
                        print('|  p or P  |  Print the position of encoder 1                     |')
                        print('|  d or D  |  Print the delta (speed) of encoder 1                |')
                        print('|  v or V  |  Print out velocity of encoder 1                     |')
                        print('|    m     |  Enter specified duty cycle for motor 1              |')
                        print('|    M     |  Enter specified duty cycle for motor 2              |')
                        print('|  c or C  |  Clear a fault condition triggered by the DRV 8847   |')
                        print('|  g or G  |  Collect data from encoder 1 for 30 seconds          |')
                        print('|  t or T  |  Start Testing Interface                             |')
                        print('|  s or S  |  End data collection prematurely                     |')
                        print('|  h or H  |  Print this help message                             |')
                        print('|  Ctrl-C  |  Terminate Program                                   |')
                        print("+-----------------------------------------------------------------+")
                        
                    elif charIn[0] == 3: # checks for ctrl-c
                        print('Terminating program')
                    else:
                        print(f"You typed invalid {charIn} from state 0.")
            
            
# State 2 code - Zero encoder position state
            elif state == S2_zZeroEnc1:
                if not zFlag.read():
                    print('zFlag is zero')
                    state = S1_Wait

# State 3 code - Position print state
            elif state == S3_pPrintPos:
                print(posDATA.read()) # prints position value
                state = S1_Wait

# State 4 code - Delta print state
            elif state == S4_dDelta:
                print(delDATA.read()) # prints delta value
                state = S1_Wait
                
# State 5 code - Instantaneous velocity print state
            elif state == S5_vVelocity:
                print(speedDATA.read()) # prints velocity value
                state = S1_Wait

# State 6 code - Set motor duty cycle(s) state
            elif state == S6_mDutyCtrl:
                numstr = ''
                enter = False
                while enter == False:
                    if serport.any():
                        charDuty = serport.read(1).decode()
                        if charDuty.isdigit():
                            numstr += charDuty # everytime charIn is looped appends to string
                            print(numstr)
                        elif charDuty == '-':
                            if len(numstr) == 0:
                                numstr += charDuty
                                print(numstr)

                        elif charDuty in {'\b', '\x08', '\x7f'}: # for backspace
                            if len(numstr)>0:
                                numstr = numstr[:-1]
                                print(numstr)
                            else:
                                pass
                            
                        elif charDuty == ' ':
                            print('User input space - Invalid input, please input a number between -100 and 100')
                            pass
                        elif charDuty == '.':
                            if numstr.count('.')==0:
                                numstr += charDuty
                            print(numstr)
                            
                        elif charDuty in {'\r', '\n'}: # for enter 
                            if len(numstr) > 0:
                                numstr = round(float(numstr),1)
                                if numstr >= 100:
                                    numstr = 100
                                elif numstr <= -100:
                                    numstr = -100
                            elif len(numstr) == 0:
                                print('Invalid input. Type a value between -100 and 100, then press Enter')
                                break
                            enter = True
                            # this allowed me to remove state 7
                            if mFlag == True:
                                print('Duty Cycle for Motor 1 has been set to', numstr)
                                duty1.write(numstr)
                                mFlag = False
                                numstr = ''
                                state = S1_Wait 
                                
                            elif MFlag == True:
                                print('Duty Cycle for Motor 2 has been set to', numstr)
                                duty2.write(numstr)
                                MFlag = False
                                numstr = ''
                                state = S1_Wait 
                            break
# State 4 code - Clear Motor Fault State
            elif state == S7_clrFault:
                faultFlag.write(False)
                print('Motor fault has been cleared')
                state = S1_Wait
            
# State 8 code - Data Collection State
            elif state == S8_gCollectThirtySec:             
                if numItems < totItems:
                   timeArray[numItems], posArray[numItems], velArray[numItems] = DATA.read()
                   numItems += 1
                   if serport.any():
                       charIn = serport.read(1).decode()
                       if charIn in {'s', 'S'}:
                           print('Ending data collection Early')
                           print('---------------Data Collection Results---------------')
                           print('Time [s],Position [rad],Velocity [rad/s]')
                           state = S10_PrintData # transition to state 11
                if numItems == totItems:
                    print('---------------Data Collection Results---------------')
                    print('Time [s],Position [rad],Velocity [rad/s]')
                    state = S10_PrintData
                    
# Testing Interface states
# State 9 code - Testing interface initiation state
            elif state == S9_tTestInt:
                state = S11_testingDutySet
                
# State 10 code - Printing State
            elif state == S10_PrintData:
                enter = False
                if encDataFlag:
                    if numPrint<numItems:
                        print(f'{((timeArray[numPrint]-timeArray[0])/1000):.2f},{(posArray[numPrint]):.2f},{(velArray[numPrint]):.2f}')
                        numPrint += 1
                        
                    elif numPrint == numItems:
                        numPrint = 0
                        numItems = 0
                        state = S1_Wait
                        encDataFlag = False
                        
                # configure for testing state
                elif testDataFlag:
                    if numPrint<numPrintTest:
                        print(f'{(posArray[numPrint]):.2f},{(velArray[numPrint]):.2f}')
                        numPrint+=1
                    elif numPrint == numPrintTest:
                        numItems = 0
                        numPrint = 0
                        numPrintTest = 0
                        state = S1_Wait
                        testDataFlag = False
                                        
# State 11 code - Duty cycle setting within testing interface state loop
            elif state == S11_testingDutySet:
                print('Enter a duty cycle for motor 1 or press (s) to exit the testing interface')
                numstr = ''
                velList = []
                enter = False
                while enter == False:
                    if faultFlag.read()==True:
                        if faultPrintBlocker == False:
                            print('Fault Detected! Press (c) to clear')
                            faultPrintBlocker = True
                    if serport.any():
                         charDuty = serport.read(1).decode()
                         if charDuty.isdigit():
                             numstr += charDuty # everytime charIn is looped appends to string
                             print(numstr)
                         elif charDuty in {'s','S'}:
                             enter = True
                             state = S10_PrintData
                             break
                         elif charDuty in {'c','C'}:
                             faultPrintBlocker = False
                             faultFlag.write(False)
                             print('Fault cleared')
                         elif charDuty == '-':
                             if len(numstr) == 0:
                                 numstr += charDuty
                                 print(numstr)
                         elif charDuty in {'\b', '\x08', '\x7f'}: # for backspace
                             if len(numstr)>0:
                                 numstr = numstr[:-1]
                                 print(numstr)
                             else:
                                 pass
                         elif charDuty == ' ':
                             print('User input space - Invalid input, please input a number between -100 and 100')
                             pass
                         elif charDuty == '.':
                             numstr += charDuty
                         elif charDuty in {'\r', '\n'}: # for enter 
                             if len(numstr) > 0:
                                 numstr = round(float(numstr),1)
                                 if numstr >= 100:
                                     numstr = 100
                                 elif numstr <= -100:
                                     numstr=-100
                                 enter = True
                                 print('Duty Cycle for Motor 1 has been set to', numstr)
                                 duty1.write(numstr)
                                 state = S12_testingDataCrunch
                                 velList = []
                                 break
                             elif len(numstr) == 0:
                                 enter = False
                                 print('Invalid input. Type a value between -100 and 100, then press Enter')
                                 break
                             
# State 12 code - Velocity testing data calculation and storage state     
            elif state == S12_testingDataCrunch:
                enter = False
                if numItems<testItems:
                    velList.append(speedDATA.read())
                    numItems += 1
                    
                elif numItems >= testItems:
                    velAvg = sum(velList[-50:])/50
                    print(velAvg)
                    velArray[numPrintTest] = float(velAvg)
                    posArray[numPrintTest] = numstr
                    numItems = 0
                    numPrintTest+=1
                    print(f'average velocity for is {velAvg}')
                    state = S11_testingDutySet
            
            else:
                raise ValueError(f"Invalid state value in {taskName}: State {state} does not exist")
            yield state
        else:
            yield None

# The following code would usually belong in main.py but is utilized here to
# test the task in isolation from other tasks. Eventually this will not be
# practical once the tasks interface with eachother using shared variables.
# if __name__ == '__main__':
    
#     # A generator object created from the generator function for the user
#     # interface task.
#     task1 = taskUserFcn('T1', 10_000, zFlag)
    
#     # To facilitate running of multiple tasks it makes sense to create a task
#     # list that can be iterated through. Adding more task objects to the list
#     # will allow the tasks to run together cooperatively.
#     taskList = [task1]
    
#     # The system should run indefinitely until the user interrupts program flow
#     # by using a Ctrl-C to trigger a keyboard interrupt. The tasks are run by
#     # using the next() function on each item in the task list.
#     while True:
#         try:
#             for task in taskList:
#                 next(task)
        
#         except KeyboardInterrupt:
#             break
            
#     # In addition to exiting the program and informing the user any cleanup code
#     # should go here. An example might be un-configuring certain hardware
#     # peripherals like the User LED or perhaps any callback functions.
#     print('Program Terminating')
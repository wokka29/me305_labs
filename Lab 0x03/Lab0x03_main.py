"""!@file Lab0x03_main.py
    @brief Main script of ME305 Lab0x03. 
    @details This file instantiates the shared flag variables, shared data variables, 
    and controls the task sharing between taskEncoder.py and taskUser.py.
    @author: Logan Williamson
    @author: Brianna Roberts
    @date 01/27/2022
"""

import shares, Lab0x03_taskUser, Lab0x03_taskEncoder, Lab0x03_taskMotor

# Flags:
    
##  @brief Create object zFlag shared between all files.
#   @details Used as a flag between files to communicate when z or Z is pressed. 
#   also used in order to communicate between tasks.
zFlag = shares.Share(False)

##  @brief Create object zFlag shared between all files.
#   @details Used as a flag between files to communicate when z or Z is pressed. 
#   also used in order to communicate between tasks.
faultFlag = shares.Share(False)



''' Shared Variables from ENCODER '''

##  @brief Create an object timDATA shared between all files
#   @details This is the value of time used in taskEncoder to keep track of
#   time. Set to timDATA.write(ticks_ms()-init_time)
timDATA = shares.Share(0)

##  @brief Create an object posDATA shared between all files
#   @details This is the value of position that is written in the taskEncoder 
#   and printed in the taskUser when prompted
posDATA = shares.Share(0) 

##  @brief Create an object delDATA shared between all files
#   @details This is the value of delta that is written in the taskEncoder 
#   and printed in the taskUser when prompted
delDATA = shares.Share(0)

##  @brief Create an object DATA shared between all files.
#   @details This is a tuple that records the data and the position. used when 
#   user prompts to ask for 30 second data collection. This is shared with taskUser
#   and formatted to print in an array. 
DATA = shares.Share(0)   

speedDATA = shares.Share(0)


''' Shared Variables from MOTOR '''
##  @brief Create an object duty1 shared between all files.
#   @details This is a value that will be passed into the motor.Motor objects 
#   for the purpose of setting the duty cycle (and by extension, the speed) of 
#   the
duty1 = shares.Share(0)

##  @brief Create an object DATA shared between all files.
#   @details This is a tuple that records the data and the position. used when 
#   user prompts to ask for 30 second data collection. This is shared with taskUser
#   and formatted to print in an array. 
duty2 = shares.Share(0)


# NOTE: So far, made changes to taskUser: Finished asking for duty cycle input 
# taskMotor: got rid of dutyFlags 1 and 2

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is imported as
    # a module the code block will not run.

    taskList = [Lab0x03_taskUser.taskUserFcn ('Task User', 10_000, zFlag, timDATA, posDATA, delDATA, DATA, speedDATA, faultFlag, duty1, duty2),
                    Lab0x03_taskEncoder.taskEncoderFcn ('Task Encoder', 10_000, zFlag, timDATA, posDATA, delDATA, DATA, speedDATA),
                    Lab0x03_taskMotor.taskMotorFcn ('Task Motor', 10_000, faultFlag, duty1, duty2)]
    
        
    while True: 
        try:
            for task in taskList:
                next(task)
                
        except KeyboardInterrupt:
            break
        
    print('Program Terminating')
        
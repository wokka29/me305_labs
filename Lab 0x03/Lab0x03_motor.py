'''!@file       Lab0x03_motor.py
    @brief      A driver for controlling motor objects
    @details    This file contains a class which can be called to instantiate motor objects.
    @author     Logan Williamson
    @author     Brianna Roberts
    @date       02/03/2022
'''
from pyb import Pin, Timer

class Motor:
    '''!@brief A motor class for one channel of the DRV8847.
        @details Objects of this class can be used to apply PWM to a given
        DC motor. This effectively controls the speed of that motor due to 
        the fact that motors work as low-pass filters.
    '''
    def __init__ (self, PWM_tim, IN1_pin, IN2_pin, chA, chB):
        '''!@brief Initializes and returns an object associated with a DC Motor.
            @details Objects of this class should not be instantiated
            directly. Instead create a DRV8847 object and use
            that to create Motor objects using the method
            DRV8847.motor(). This will allow higher-level management of these 
            motor objects by the task files that will utilize and 
            manipulate them.
            @param PWM_tim  Creates empty object for an input of pin position for the
                            pin we desire to control all four channels of our pulse width modulation. 
                            In this case, we are using timer 3 on the STM32 Nucleo.
            @param IN1_pin  Creates empty object for an input of pin position. 
                            The pin passed into this parameter will be the first of two pins required to
                            control the associated motor object instantiated by this class.
            @param IN2_pin  Creates empty object for an input of pin position. 
                            The pin passed into this parameter will be the second of two pins required to
                            control the associated motor object instantiated by this class.
            @param chA      Creates empty object for an input of a pin channel. The pin channels will 
                            control the direction of rotation of the motor based on the sign of the duty
                            cycle input to the set_duty method.
            @param chB      Creates empty object for an input of a pin channel. The pin channels will 
                            control the direction of rotation of the motor based on the sign of the duty
                            cycle input to the set_duty method.
        '''
        
        self.tim = Timer(PWM_tim,freq = 20_000)
        self.Pin1 = IN1_pin
        self.Pin2 = IN2_pin
        self.tim_ch1 = self.tim.channel(chA,Timer.PWM_INVERTED,pin=IN1_pin)
        self.tim_ch2 = self.tim.channel(chB,Timer.PWM_INVERTED,pin=IN2_pin)
        self.tim_ch1.pulse_width_percent(0)
        self.tim_ch2.pulse_width_percent(0)
        # self.duty1 = duty1.read() # Added this value
        # self.duty2 = duty2.read() # Added this value

    def set_duty (self, duty):
        '''!@brief Set the PWM duty cycle for the motor channel.
            @details This method sets the duty cycle to be sent
            to the motor to the given level. Positive values
            cause effort in one direction, negative values
            in the opposite direction.
            @param duty A signed number holding the duty
            cycle of the PWM signal sent to the motor
        '''
        if duty<0:
            self.tim_ch1.pulse_width_percent(0)
            self.tim_ch2.pulse_width_percent(-duty)
        elif duty == 0:
            self.tim_ch1.pulse_width_percent(0)
            self.tim_ch2.pulse_width_percent(0)
        elif duty>0:
            self.tim_ch1.pulse_width_percent(duty)
            self.tim_ch2.pulse_width_percent(0)
        pass

if __name__ == '__main__':
# Adjust the following code to write a test program for your motor class. Any
# code within the if __name__ == '__main__' block will only run when the
# script is executed as a standalone program. If the script is imported as
# a module the code block will not run.

    # Example input values for instantiation of motor object
        # PWM_tim = 3
        # IN1_pin = Pin.cpu.B4
        # IN2_pin = Pin.cpu.B5
        # chA = 1
        # chB = 2
    # Create a motor driver object and two motor objects. You will need to
    # modify the code to facilitate passing in the pins and timer objects needed
    # to run the motors.
    motor_1 = Motor(3, Pin.cpu.B4, Pin.cpu.B5, 1, 2)
    motor_2 = Motor(3, Pin.cpu.B0, Pin.cpu.B1, 3, 4)
    
    # Enable the motor driver
    nSLEEP = Pin(Pin.cpu.A15, mode=Pin.OUT_PP)
    nFAULT = Pin(Pin.cpu.A6, mode=Pin.OUT_PP)
    nSLEEP.high()
    
    # Set the duty cycle of the first motor to 40 percent
    motor_1.set_duty(0)
    motor_2.set_duty(0)